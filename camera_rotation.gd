extends Camera

var m_rotation: Vector3 = Vector3()
var m_xspeed: float = .5
var m_yspeed: float = .15
var m_zspeed: float = .1

func _process(delta: float):
	m_rotation.x += m_xspeed * delta
	m_rotation.y += m_yspeed * delta
	m_rotation.z += m_zspeed * delta
	transform = Transform(Quat(m_rotation))
