extends KinematicBody

class MovementSettings:
	var mspeed: float = 0
	var accel: float = 0
	var deaccel: float = 0

	func _init(mspeed: float, accel: float, deaccel: float):
		self.mspeed = mspeed;
		self.accel = accel
		self.deaccel = deaccel

export(float) var sensitivity = 0.1

var m_friction: float = 6
var m_gravity: float = 20
var m_jump_force: float = 8
var m_bunny_hop: bool = false
var m_air_control: float = 0.3
var m_crouch_control: float = 0.5

var m_ground_settings: MovementSettings = MovementSettings.new(7, 14, 10)
var m_crouch_settings: MovementSettings = MovementSettings.new(4.5, 28, 10)
var m_air_settings: MovementSettings    = MovementSettings.new(7, 2, 2)
var m_strafe_settings: MovementSettings = MovementSettings.new(1, 50, 50)

var m_move_direction_normal: Vector3 = Vector3.ZERO
var m_player_velocity: Vector3 = Vector3.ZERO
var m_gravity_velocity: Vector3 = Vector3.ZERO

var m_jump_queued: bool = false

var m_player_friction: float = 0

var m_move_input: Vector3 = Vector3.ZERO
var m_weapon_sway: float = 16
var m_weapon_vsway: float = 8

var m_is_crouching: bool = false
var m_crouch_speed: float = 8.0
var m_crouch_height: float = 0.3
var m_normal_height: float = 0.9
var m_height_calculated_for_division: float = 1 / (m_normal_height - m_crouch_height)
var m_crouch_cam_height: float = 0.3
var m_normal_cam_height: float = 0.65

var m_explosion_force: float = 16.0
var m_rocket_speed: float = 2.0
var m_exploded: bool = false

onready var m_pitch = $yaw/pitch
onready var m_ground_check = $ground_check
onready var m_ceiling_check = $ceiling_check
onready var m_body = $body
onready var m_mesh = $mesh
onready var m_hand = $yaw/pitch/viewport/hand
onready var m_hand_loc = $yaw/pitch/hand_loc

onready var m_camera = $yaw/pitch/camera
onready var m_vp_camera = $yaw/pitch/viewport/camera
onready var m_rocket_ray = $yaw/pitch/camera/rocket_ray
onready var m_info = $ui/info_panel
onready var m_info_text = $ui/info_panel/text
onready var m_crosshair = $ui/crosshair

onready var m_rocket_explosion_scn = preload("res://rocket_explosion.tscn")

var m_respawn_point: Transform

var m_info_shown: bool = false

var m_current_viewport_size: Vector2 = Vector2.ZERO

func show_info(info: String):
	m_info_text.bbcode_text = info
	m_info_shown = true
	m_info.visible = true

func _ready():
	m_bunny_hop = Global.auto_jump
	m_camera.fov = Global.fov
	m_vp_camera.fov = Global.fov
	m_crosshair.visible = Global.crosshair
	m_current_viewport_size = get_viewport().size
	$yaw/pitch/viewport.size = get_viewport().size
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	m_hand.set_as_toplevel(true)
	m_respawn_point = get_global_transform()

func set_respawn(trans: Transform):
	if m_respawn_point != trans:
		m_respawn_point = trans
		$ui/checkpoint_reached.visible = true
		yield(get_tree().create_timer(2.0), "timeout")
		$ui/checkpoint_reached.visible = false

func respawn():
	set_global_transform(m_respawn_point)
	m_gravity_velocity = Vector3()
	m_player_velocity = Vector3()

func _process(delta: float):
	m_vp_camera.global_transform.basis = m_camera.global_transform.basis
	m_vp_camera.global_transform.origin = m_camera.global_transform.origin

	m_hand.global_transform.origin = m_hand_loc.global_transform.origin
	m_hand.rotation.y = lerp_angle(m_hand.rotation.y, rotation.y, m_weapon_sway * delta)
	m_hand.rotation.x = lerp_angle(m_hand.rotation.x, m_pitch.rotation.x, m_weapon_vsway * delta)

	if m_info_shown:
		if Input.is_action_just_pressed("mv_crouch") or Input.is_action_just_pressed("mv_down") or Input.is_action_just_pressed("mv_jump") or Input.is_action_just_pressed("mv_left") or Input.is_action_just_pressed("mv_right") or Input.is_action_just_pressed("mv_up") or Input.is_action_just_pressed("shoot"):
			m_info_shown = false
			m_info.visible = false

	if m_current_viewport_size != get_viewport().size:
		m_current_viewport_size = get_viewport().size
		$yaw/pitch/viewport.size = get_viewport().size

	if transform.origin.y <= -100:
		respawn()

func explosion():
	if m_rocket_ray.is_colliding():
		var origin: Vector3 = m_rocket_ray.global_transform.origin
		var collision_point: Vector3 = m_rocket_ray.get_collision_point()
		var distance: float = origin.distance_to(collision_point)

		var rocket: Node = m_rocket_explosion_scn.instance()
		rocket.global_translate(collision_point)
		rocket.m_spawn_timer_time = (distance * 0.1)
		$explosion_handler.add_child(rocket)

func _physics_process(delta: float):
	get_input()

	queue_jump()

	crouching(delta)

	if m_exploded and not $explosion_timer.time_left > 0:
		$explosion_timer.start()

	if is_on_floor() and not m_exploded:
		m_player_velocity.y = 0
		if m_is_crouching:
			crouch_move(delta)
		else:
			ground_move(delta)
	else:
		air_move(delta)

	if is_on_wall():
		var normal = Vector3.ZERO
		for i in get_slide_count():
			normal += get_slide_collision(i).normal
		normal.y = 0
		m_player_velocity += normal

	if Input.is_action_just_pressed("shoot"):
		explosion()

	var movement: Vector3 = m_player_velocity + m_gravity_velocity
	move_and_slide(movement, Vector3.UP)

	$ui/fps.set_text(str("fps: ", Performance.get_monitor(Performance.TIME_FPS)))
#	$ui/tps.set_text(str("physics_time: ", Performance.get_monitor(Performance.TIME_PHYSICS_PROCESS)))
#	$ui/on_ground.set_text(str("on ground: ", is_on_floor()))
#	$ui/on_ceiling.set_text(str("on ceiling: ", m_ceiling_check.is_colliding()))
#	$ui/queued_next_jump.set_text(str("queued next jump: ", m_jump_queued))
#	$ui/direction.set_text(str("direction: ", m_player_velocity.normalized()))
#	$ui/speed.set_text(str("horizontal speed: ", Vector2(m_player_velocity.x, m_player_velocity.z).length()))
	$ui/speed_bar.value = Vector2(m_player_velocity.x, m_player_velocity.z).length() * 16
	$ui/speed_bar/label.set_text(str(Vector2(m_player_velocity.x, m_player_velocity.z).length() * 16))
#	$ui/upmove_speed.set_text(str("vertical speed: ", m_player_velocity.y))
#	$ui/friction.set_text(str("friction: ", m_friction))

func crouching(delta: float):
	m_is_crouching = Input.is_action_pressed("mv_crouch")

	if m_is_crouching:
		m_body.shape.height -= m_crouch_speed * delta
		m_mesh.mesh.mid_height = m_body.shape.height
	elif not m_ceiling_check.is_colliding() and not m_body.shape.height == m_normal_height:
		m_body.shape.height += m_crouch_speed * delta
		m_mesh.mesh.mid_height = m_body.shape.height

	m_body.shape.height = clamp(m_body.shape.height, m_crouch_height, m_normal_height)

	var percentage: float = ((m_body.shape.height - m_crouch_height) * m_height_calculated_for_division)
	
	var cam_height: float = m_crouch_height + ((m_normal_cam_height - m_crouch_cam_height) * percentage)
	m_pitch.transform.origin.y = cam_height

func get_input():
	m_move_input = Vector3.ZERO
	if Input.is_action_pressed("mv_up"):
		m_move_input.z -= 1;
	if Input.is_action_pressed("mv_down"):
		m_move_input.z += 1;
	if Input.is_action_pressed("mv_left"):
		m_move_input.x -= 1;
	if Input.is_action_pressed("mv_right"):
		m_move_input.x += 1;

func accelerate(delta: float, target_dir: Vector3, target_speed: float, accel: float):
	var cspeed: float = m_player_velocity.dot(target_dir)
	var add_speed: float = target_speed  - cspeed
	if add_speed <= 0:
		return

	var accel_speed: float = accel * delta * target_speed
	if accel_speed > add_speed:
		accel_speed = add_speed

	m_player_velocity.x += accel_speed * target_dir.x
	m_player_velocity.z += accel_speed * target_dir.z

func apply_friction(delta: float, t: float, settings: MovementSettings):
	var vec: Vector3 = m_player_velocity
	vec.y = 0
	var speed: float = vec.length()
	var drop: float = 0

	if is_on_floor():
		var control: float = 0
		if speed < settings.deaccel:
			control = settings.deaccel
		else:
			control = speed
		drop = control * m_friction * delta * t

	var nspeed: float = speed - drop
	m_player_friction = nspeed
	if nspeed < 0:
		nspeed = 0

	if speed > 0:
		nspeed /= speed

	m_player_velocity.x *= nspeed
	m_player_velocity.z *= nspeed

func air_control(delta: float, target_dir: Vector3, target_speed: float):
	if abs(m_move_input.z) < 0.001 || abs(target_speed) < 0.001:
		return

	var zspeed: float = m_player_velocity.y
	m_player_velocity.y = 0

	var speed: float = m_player_velocity.length()
	m_player_velocity = m_player_velocity.normalized()

	var dot: float = m_player_velocity.dot(target_dir)
	var k: float = 32
	k *= m_air_control * dot * dot * delta

	if dot > 0:
		m_player_velocity.x *= speed + target_dir.x * k
		m_player_velocity.y *= speed + target_dir.y * k
		m_player_velocity.z *= speed + target_dir.z * k

		m_player_velocity = m_player_velocity.normalized()
		m_move_direction_normal = m_player_velocity

	m_player_velocity.x *= speed
	m_player_velocity.y = zspeed
	m_player_velocity.z *= speed

func ground_move(delta: float):
	if not m_jump_queued:
		apply_friction(delta, 1.0, m_ground_settings)
	else:
		apply_friction(delta, 0.0, m_ground_settings)

	var wish_dir: Vector3 = Vector3.ZERO
	wish_dir += get_global_transform().basis.z.normalized() * m_move_input.z
	wish_dir += get_global_transform().basis.x.normalized() * m_move_input.x

	wish_dir = wish_dir.normalized()
	m_move_direction_normal = wish_dir

	var wish_speed: float = wish_dir.length()
	wish_speed *= m_ground_settings.mspeed

	accelerate(delta, wish_dir, wish_speed, m_ground_settings.accel)

	if m_ground_check.is_colliding():
		m_gravity_velocity = -get_floor_normal().normalized() * m_gravity
	else:
		m_gravity_velocity = -get_floor_normal().normalized()

	if m_jump_queued:
		$jump_sfx.play()
		m_gravity_velocity = Vector3.UP * m_jump_force
		m_jump_queued = false

func crouch_move(delta: float):
	if Vector2(m_player_velocity.x, m_player_velocity.z).length() > 9.0:
		apply_friction(delta, 0.4, m_crouch_settings)
	else:
		apply_friction(delta, 1.0, m_crouch_settings)

	var wish_dir: Vector3 = Vector3.ZERO
	wish_dir += get_global_transform().basis.z.normalized() * m_move_input.z
	wish_dir += get_global_transform().basis.x.normalized() * m_move_input.x

	wish_dir = wish_dir.normalized()
	m_move_direction_normal = wish_dir

	var wish_speed: float = wish_dir.length()
	wish_speed *= m_crouch_settings.mspeed

	accelerate(delta, wish_dir, wish_speed, m_crouch_settings.accel)

	if m_ground_check.is_colliding():
		m_gravity_velocity = -get_floor_normal().normalized() * m_gravity
	else:
		m_gravity_velocity = -get_floor_normal().normalized()

func air_move(delta: float):
	var accel: float = 0

	if is_on_ceiling() and m_gravity_velocity.y > 0:
		m_gravity_velocity.y = -1

	var wish_dir: Vector3 = Vector3()
	wish_dir += get_global_transform().basis.z.normalized() * m_move_input.z
	wish_dir += get_global_transform().basis.x.normalized() * m_move_input.x

	var wish_speed: float = wish_dir.length()
	wish_speed *= m_air_settings.mspeed

	wish_dir = wish_dir.normalized()
	m_move_direction_normal = wish_dir

	var wish_speed2: float = wish_speed
	if m_player_velocity.dot(wish_dir) < 0:
		accel = m_air_settings.deaccel
	else:
		accel = m_air_settings.accel

	if m_move_input.z == 0 and m_move_input.x != 0:
		if wish_speed > m_strafe_settings.mspeed:
			wish_speed = m_strafe_settings.mspeed
		accel = m_strafe_settings.accel

	accelerate(delta, wish_dir, wish_speed, accel)
	if m_air_control > 0:
		air_control(delta, wish_dir, wish_speed2)

	m_gravity_velocity += Vector3.DOWN * m_gravity * delta

func queue_jump():
	if m_bunny_hop:
		m_jump_queued = Input.is_action_pressed("mv_jump")
		return
	if Input.is_action_just_pressed("mv_jump") and not m_jump_queued:
		m_jump_queued = true
	elif Input.is_action_just_released("mv_jump"):
		m_jump_queued = false

## Input
func _input(event: InputEvent):
	if event is InputEventKey:
		# mouse capture and release
		if event.pressed and event.scancode == KEY_ESCAPE:
			if Input.get_mouse_mode() == Input.MOUSE_MODE_CAPTURED:
				Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
			else:
				Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)

	# Mouse camera movement
	if event is InputEventMouseMotion and Input.get_mouse_mode() == Input.MOUSE_MODE_CAPTURED:
		m_pitch.rotate_x(deg2rad(event.relative.y * (sensitivity * Global.sensitivity) * -1))
		rotate_y(deg2rad(event.relative.x * (sensitivity * Global.sensitivity) * -1))

		var camera_rot = m_pitch.rotation_degrees
		camera_rot.x = clamp(camera_rot.x, -89, 89)
		m_pitch.rotation_degrees = camera_rot

func _on_explosion_timer_timeout():
	m_exploded = false
