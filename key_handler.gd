extends Panel

func _process(_delta):
	if Input.is_action_pressed("mv_up"):
		$w/dark.visible = true
		$w/light.visible = false
	else:
		$w/dark.visible = false
		$w/light.visible = true
	if Input.is_action_pressed("mv_down"):
		$s/dark.visible = true
		$s/light.visible = false
	else:
		$s/dark.visible = false
		$s/light.visible = true
	if Input.is_action_pressed("mv_left"):
		$a/dark.visible = true
		$a/light.visible = false
	else:
		$a/dark.visible = false
		$a/light.visible = true
	if Input.is_action_pressed("mv_right"):
		$d/dark.visible = true
		$d/light.visible = false
	else:
		$d/dark.visible = false
		$d/light.visible = true
	if Input.is_action_pressed("mv_jump"):
		$space/dark.visible = true
		$space/light.visible = false
	else:
		$space/dark.visible = false
		$space/light.visible = true
	if Input.is_action_pressed("mv_crouch"):
		$ctrl/dark.visible = true
		$ctrl/light.visible = false
	else:
		$ctrl/dark.visible = false
		$ctrl/light.visible = true
