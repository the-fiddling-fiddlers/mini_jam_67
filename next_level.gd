extends Position3D

export(Dictionary) var properties setget set_properties

func set_properties(new_properties: Dictionary) -> void:
	if properties != new_properties:
		properties = new_properties

func use() -> void:
	next_level()

func next_level() -> void:
	print("Loading Next Level...")
