extends Node

func _ready():
	connect("body_entered", self, "handle_body_entered")

func handle_body_entered(body: Node):
	if body.is_in_group("player"):
		body.reset()
