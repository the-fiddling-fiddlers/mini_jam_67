extends Spatial

var m_spawn_timer_time: float = 1
var m_started: bool = false

func spawn_timer_end():
	$particles.restart()
	$spawn_timer.stop()
	m_started = true

	for body in $area.get_overlapping_bodies():
		if body.is_in_group("player"):
			var origin: Vector3 = global_transform.origin
			var player_origin: Vector3 = body.global_transform.origin
			var distance: float = origin.distance_to(player_origin)
			
			var direction: Vector3 = origin.direction_to(player_origin).normalized()
			var explosion: Vector3 = direction * body.m_explosion_force
			body.m_player_velocity.x += explosion.x
			body.m_gravity_velocity = Vector3.UP * explosion.y
			body.m_player_velocity.z += explosion.z
			body.m_exploded = true

func _ready():
	$spawn_timer.start(m_spawn_timer_time)

func _process(delta):
	if not $particles.is_emitting() and m_started:
		queue_free()
