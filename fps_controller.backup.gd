extends KinematicBody

var sensitivity = 0.1

var gravity = 19.6
var friction = 8.0

var move_speed = 7.0

var run_accel = 14.0
var run_deaccel = 10.0

var crouch_accel = 2.0
var crouch_deaccel = 0.0

var air_accel = 2.0
var air_deaccel = 2.0
var air_control = 0.3

var side_strafe_accel = 50.0
var side_strafe_speed = 1.0

var jump_speed = 8.0
var wall_jump_speed = 32.0
var move_scale = 1.0

var crouch_speed = 8.0
var crouch_height = 0.3
var normal_height = 0.9
var normal_cam_height = 0.65
var crouch_cam_height = 0.3

var move_cmd = Vector2(0, 0)

var move_dir_norm = Vector3(0, 0, 0)
var player_velocity = Vector3(0, 0, 0)
var player_top_velocity = 0.0

var wish_jump = false
var player_friction = 0.0

var crouching = false

var start_pos = Vector3()

var gravity_vec = Vector3()
var full_contact = false

onready var ground_check = $ground_check
onready var ceiling_check = $ceiling_check

onready var pcap = $body
onready var mesh = $mesh

func _ready():
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	start_pos = transform.origin

func reset():
	transform.origin = start_pos
	player_friction = 0
	player_velocity = Vector3()

func _physics_process(delta):
	# check if the spacebar is being held so we can bunnyhop the moment we touch the ground without losing speed to friction
	queue_jump()
	check_crouching(delta)

	full_contact = ground_check.is_colliding()

	# Check if we need to use ground or air logic
	if not is_on_floor():
		gravity_vec += Vector3.DOWN * gravity * delta
		air_move(delta)
	elif full_contact:
		gravity_vec = -get_floor_normal() * gravity
		ground_move(delta)
	else:
		gravity_vec = -get_floor_normal()
		ground_move(delta)

	if wish_jump and (is_on_floor() or full_contact):
		gravity_vec = Vector3.UP * jump_speed
		wish_jump = false

	var movement = player_velocity + gravity_vec

	# move the KinematicBody
	move_and_slide(movement, Vector3(0, 1, 0))

	# Update UI
	$ui/fps.set_text(str("frame_time: ", Performance.get_monitor(Performance.TIME_PROCESS)))
	$ui/tps.set_text(str("physics_time: ", Performance.get_monitor(Performance.TIME_PHYSICS_PROCESS)))
	$ui/on_ground.set_text(str("on ground: ", is_on_floor()))
	$ui/on_ceiling.set_text(str("on ceiling: ", ceiling_check.is_colliding()))
	$ui/queued_next_jump.set_text(str("queued next jump: ", wish_jump))
	$ui/direction.set_text(str("direction: ", player_velocity.normalized()))
	$ui/speed.set_text(str("horizontal speed: ", Vector2(movement.x, movement.z).length()))
	$ui/upmove_speed.set_text(str("vertical speed: ", movement.y))
	$ui/friction.set_text(str("friction: ", player_friction))

## update move_cmd Vector2
func set_move_dir():
	move_cmd = Vector2()
	if Input.is_action_pressed("mv_up"):
		move_cmd.y += 1
	if Input.is_action_pressed("mv_down"):
		move_cmd.y -= 1
	if Input.is_action_pressed("mv_left"):
		move_cmd.x -= 1
	if Input.is_action_pressed("mv_right"):
		move_cmd.x += 1

## apply friction with strength t
func apply_friction(delta, t):
	var vec = player_velocity
	var speed = 0
	var new_speed = 0
	var control = 0
	var drop = 0

	vec.y = 0
	speed = vec.length()

	if is_on_floor():
		if crouching:
			if speed < crouch_deaccel:
				control = crouch_deaccel
			else:
				control = speed
		else:
			if speed < run_deaccel:
				control = run_deaccel
			else:
				control = speed
		drop = control * friction * delta * t

	new_speed = speed - drop
	player_friction = new_speed
	if new_speed < 0:
		new_speed = 0
	if speed > 0:
		new_speed /= speed

	player_velocity.x *= new_speed
	player_velocity.z *= new_speed

## Accelerate the player_velocity into the given direction with speed and accel
func accelerate(delta, wish_dir, wish_speed, accel):
	var add_speed = 0
	var accel_speed = 0
	var current_speed = 0

	current_speed = player_velocity.dot(wish_dir)
	add_speed = wish_speed - current_speed
	if add_speed <= 0:
		return

	accel_speed = accel * delta * wish_speed
	if accel_speed > add_speed:
		accel_speed = add_speed

	player_velocity.x += accel_speed * wish_dir.x
	player_velocity.z += accel_speed * wish_dir.z

## Ground movement Logic
func ground_move(delta):
	var wish_dir = Vector3()

	# Do not apply friction if we are queuing for another jump
	if wish_jump:
		apply_friction(delta, 0.1)
	elif crouching and Vector2(player_velocity.x, player_velocity.z).length() > 9.0:
		apply_friction(delta, 0.5)
	else:
		apply_friction(delta, 1.0)

	set_move_dir()

	var cam_xform = get_global_transform()
	wish_dir = Vector3(0, 0, 0)

	wish_dir += -cam_xform.basis.z * move_cmd.y
	wish_dir +=  cam_xform.basis.x * move_cmd.x
	#wish_dir += -cam_xform.basis.z.normalized() * move_cmd.y
	#wish_dir +=  cam_xform.basis.x.normalized() * move_cmd.x
	#wish_dir = wish_dir.normalized() # TODO WHY IS THIS COMMENTED
	move_dir_norm = wish_dir

	var wish_speed = wish_dir.length()
	wish_speed *= move_speed

	if crouching:
		accelerate(delta, wish_dir, wish_speed, crouch_accel)
	else:
		accelerate(delta, wish_dir, wish_speed, run_accel)

## Air movement logic
func air_move(delta):
	var wish_dir = Vector3()
	var wish_vel = air_accel
	var accel = 0

	apply_friction(delta, 0.2)

	set_move_dir()

	# when we hit a wall see where that wall is in relation to the player
	# and calculate the velocity we should be losing because of this collision
	if is_on_wall():
		var norm = Vector3()
		for i in get_slide_count():
			norm += get_slide_collision(i).normal
		norm.y = 0
		player_velocity += norm

	# stop moving upwards if we hit a ceiling
	if is_on_ceiling() and gravity_vec.y > 0:
		gravity_vec.y = -1

	var cam_xform = get_global_transform()
	wish_dir = Vector3(0, 0, 0)

	wish_dir += -cam_xform.basis.z.normalized() * move_cmd.y
	wish_dir +=  cam_xform.basis.x.normalized() * move_cmd.x

	var wish_speed = wish_dir.length()
	wish_speed *= move_speed

	wish_dir = wish_dir.normalized()
	move_dir_norm = wish_dir

	var wish_speed2 = wish_speed
	if player_velocity.dot(wish_dir) < 0:
		accel = air_deaccel
	else:
		accel = air_accel

	# if the player is only strafing left/right
	if move_cmd.y == 0 and move_cmd.x != 0:
		if wish_speed > side_strafe_speed:
			wish_speed = side_strafe_speed
		accel = side_strafe_accel

	accelerate(delta, wish_dir, wish_speed, accel)
	if air_control > 0:
		air_control(delta, wish_dir, wish_speed2)

## calculate the amount of control we have in the air
func air_control(delta, wish_dir, wish_speed):
	var zspeed = 0
	var speed = 0
	var dot = 0
	var k = 0

	# Cant control movement unless we are moving forward/backwards
	if abs(move_cmd.y) < 0.001 or abs(wish_speed) < 0.001:
		return
	zspeed = player_velocity.y
	player_velocity.y = 0

	speed = player_velocity.length()
	player_velocity = player_velocity.normalized()

	dot = player_velocity.dot(wish_dir)
	k = 32
	k *= air_control * dot * dot * delta

	if dot > 0:
		player_velocity.x = player_velocity.x * speed + wish_dir.x * k
		player_velocity.y = player_velocity.y * speed + wish_dir.y * k
		player_velocity.z = player_velocity.z * speed + wish_dir.z * k

		player_velocity = player_velocity.normalized()
		move_dir_norm = player_velocity

	player_velocity.x *= speed
	player_velocity.y = zspeed
	player_velocity.z *= speed

## check if crouching
func check_crouching(delta: float):
	crouching = Input.is_action_pressed("mv_crouch")

	if crouching:
		pcap.shape.height -= crouch_speed * delta
		mesh.mesh.mid_height = pcap.shape.height
	elif not ceiling_check.is_colliding():
		pcap.shape.height += crouch_speed * delta
		mesh.mesh.mid_height = pcap.shape.height

	pcap.shape.height = clamp(pcap.shape.height, crouch_height, normal_height)

	var percentage = ((pcap.shape.height - crouch_height) / (normal_height - crouch_height))

	var cam_height = crouch_height + ((normal_cam_height - crouch_cam_height) * percentage)
	$yaw/pitch.transform.origin.y = cam_height

## check if space is held so the player can bunnyhop the instant he hits the ground
func queue_jump():
	if Input.is_action_just_pressed("mv_jump") and not wish_jump:
		wish_jump = true
	elif Input.is_action_just_released("mv_jump"):
		wish_jump = false

## Input
func _input(event):
	if event is InputEventKey:
		# mouse capture and release
		if event.pressed and event.scancode == KEY_ESCAPE:
			if Input.get_mouse_mode() == Input.MOUSE_MODE_CAPTURED:
				Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
			else:
				Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)

		# Reset player to start position
		if event.pressed and event.scancode == KEY_R:
			reset()

	# Mouse camera movement
	if event is InputEventMouseMotion and Input.get_mouse_mode() == Input.MOUSE_MODE_CAPTURED:
		$yaw/pitch.rotate_x(deg2rad(event.relative.y * sensitivity * -1))
		rotate_y(deg2rad(event.relative.x * sensitivity * -1))

		var camera_rot = $yaw/pitch.rotation_degrees
		camera_rot.x = clamp(camera_rot.x, -89, 89)
		$yaw/pitch.rotation_degrees = camera_rot
