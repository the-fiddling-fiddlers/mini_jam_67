extends Area

export(Dictionary) var properties setget set_properties

var info: String = ""

func set_properties(new_properties: Dictionary) -> void:
	if properties != new_properties:
		properties = new_properties
		update_properties()

func update_properties() -> void:
	if 'info' in properties:
		info = properties.info

func use() -> void:
	show_info()

func show_info() -> void:
	get_tree().call_group("player", "show_info", info)
