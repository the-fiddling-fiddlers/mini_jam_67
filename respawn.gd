extends Position3D

export(Dictionary) var properties setget set_properties

func set_properties(new_properties: Dictionary) -> void:
	if properties != new_properties:
		properties = new_properties

func use() -> void:
	respawn()

func set_spawn() -> void:
	get_tree().call_group("player", "set_respawn", get_global_transform())

func respawn() -> void:
	set_spawn()
	get_tree().call_group("player", "respawn")
