extends Control

onready var m_viewport = $viewport

signal start_game
signal start_tutorial
signal start_test

func _ready():
	$options/vbox_container/sensitivity/sensitivity.text = str(Global.sensitivity)
	$options/vbox_container/fov/fov.text = str(Global.fov)
	
	$options/vbox_container/fov/fov_slider.value = Global.fov
	$options/vbox_container/sensitivity/sensitivity_slider.value = Global.sensitivity
	
	$options/vbox_container/auto_jump/auto_jump.pressed = Global.auto_jump
	$options/vbox_container/crosshair/crosshair.pressed = Global.crosshair

func _process(_delta):
	if m_viewport.size != get_viewport().size:
		print(m_viewport.size)
		m_viewport.size = get_viewport().size
		print("changed to: ", m_viewport.size)

func _on_Exit_pressed():
	get_tree().quit()

func _on_Options_pressed():
	$main.visible = false
	$options.visible = true

func _on_Play_pressed():
	emit_signal("start_game")

func _on_fov_slider_value_changed(value):
	$options/vbox_container/fov/fov.text = str(value)
	Global.fov = value

func _on_sensitivity_slider_value_changed(value):
	$options/vbox_container/sensitivity/sensitivity.text = str(value)
	Global.sensitivity = value

func _on_auto_jump_toggled(button_pressed):
	Global.auto_jump = button_pressed

func _on_crosshair_toggled(button_pressed):
	Global.crosshair = button_pressed

func _on_back_pressed():
	$main.visible = true
	$options.visible = false

func _on_tutorial_pressed():
	emit_signal("start_tutorial")

func _on_test_pressed():
	emit_signal("start_test")
