extends Node

onready var main_menu = preload("res://main_menu.tscn")

onready var tutorial_level = preload("res://scenes/playground_level/level_play_ground.tscn")
onready var tower_level = preload("res://scenes/tower_level/tower_level.tscn")
onready var test_level = preload("res://scenes/test_level/test_map.tscn")

var menu: Node
var current_level: Node

func _ready():
	menu = main_menu.instance()
	add_child(menu)

	menu.connect("start_tutorial", self, "start_tutorial")
	menu.connect("start_test", self, "start_test")
	menu.connect("start_game", self, "start_game")

func start_game():
	menu.visible = false
	current_level = tower_level.instance()
	add_child(current_level)

func start_test():
	menu.visible = false
	current_level = test_level.instance()
	add_child(current_level)

func start_tutorial():
	menu.visible = false
	current_level = tutorial_level.instance()
	add_child(current_level)

func _process(delta):
	if not $music.playing:
		$music.play()
